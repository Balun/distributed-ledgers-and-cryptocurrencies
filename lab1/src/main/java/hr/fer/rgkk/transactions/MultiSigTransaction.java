package hr.fer.rgkk.transactions;

import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.crypto.TransactionSignature;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;
import org.bitcoinj.script.ScriptOpCodes;

/**
 * You must implement locking and unlocking script such that transaction output is locked by one mandatory authority
 * (e.g. bank) and at least 1 of 3 other authorities (e.g. bank associates).
 */
public class MultiSigTransaction extends ScriptTransaction {

    private final ECKey bank;

    private final ECKey first;

    private final ECKey second;

    private final ECKey third;

    public MultiSigTransaction(WalletKit walletKit, NetworkParameters parameters) {
        super(walletKit, parameters);
        this.bank = getWallet().freshReceiveKey();
        this.first = getWallet().freshReceiveKey();
        this.second = getWallet().freshReceiveKey();
        this.third = getWallet().freshReceiveKey();
    }

    @Override
    public Script createLockingScript() {
        return new ScriptBuilder()
                .number(1)
                .data(bank.getPubKey())
                .number(1)
                .op(ScriptOpCodes.OP_CHECKMULTISIGVERIFY)
                .number(1)
                .data(first.getPubKey())
                .data(second.getPubKey())
                .data(third.getPubKey())
                .number(3)
                .op(ScriptOpCodes.OP_CHECKMULTISIG)
                .build();
    }

    @Override
    public Script createUnlockingScript(Transaction unsignedTransaction) {
        TransactionSignature bankSignature = sign(unsignedTransaction, bank);
        TransactionSignature firstClientSignature = sign(unsignedTransaction,
                first);
        TransactionSignature secondClientSignature = sign(unsignedTransaction,
                second);
        TransactionSignature thirdClientSignature = sign(unsignedTransaction,
                third);

        return new ScriptBuilder()
                .number(0)
                .data(firstClientSignature.encodeToBitcoin())
                .data(secondClientSignature.encodeToBitcoin())
                .number(0)
                .data(bankSignature.encodeToBitcoin())
                .build();
    }
}
