package hr.fer.rgkk.transactions;

import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;
import org.bitcoinj.script.ScriptOpCodes;

/**
 * You must implement locking and unlocking script such that transaction output is locked by 2 integers x and y
 * such that they are solution to the equation system:
 * <pre>
 *     x + y = first four digits of your student id
 *     abs(x-y) = last four digits of your student id
 * </pre>
 * If needed change last digit of your student id such that x and y have same parity. This is needed so that equation
 * system has integer solutions.
 */
public class LinearEquationTransaction extends ScriptTransaction {

    private static final int X = 4835;

    private static final int Y = -4799;

    private static final int FIRST_4 = 36;

    private static final int LAST_4 = 9634;

    public LinearEquationTransaction(WalletKit walletKit, NetworkParameters parameters) {
        super(walletKit, parameters);
    }

    @Override
    public Script createLockingScript() {
        return new ScriptBuilder()
                .op(ScriptOpCodes.OP_2DUP)
                .op(ScriptOpCodes.OP_ADD)
                .number(FIRST_4)
                .op(ScriptOpCodes.OP_EQUALVERIFY)
                .op(ScriptOpCodes.OP_SUB)
                .op(ScriptOpCodes.OP_ABS)
                .number(LAST_4)
                .op(ScriptOpCodes.OP_EQUAL)
                .build();
    }

    @Override
    public Script createUnlockingScript(Transaction unsignedScript) {
        return new ScriptBuilder()
                .number(X)
                .number(Y)
                .build();
    }
}
