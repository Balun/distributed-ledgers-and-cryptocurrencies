pragma solidity ^0.4.24;

import "./Timer.sol";

/// This contract represents the most simple crowdfunding campaign.
/// This contract does not protect investors from not receiving goods
/// they were promised from the crowdfunding owner. This kind of contract
/// might be suitable for campaigns that do not promise anything to the
/// investors except that they will start working on the project.
/// (e.g. almost all blockchain spinoffs.)
contract Crowdfunding {

    address private owner;

    Timer private timer;

    uint256 public goal;

    uint256 public endTimestamp;

    uint256 private sum;

    mapping(address => uint256) public investments;

    constructor(
        address _owner,
        Timer _timer,
        uint256 _goal,
        uint256 _endTimestamp
    ) public {
        owner = _owner == 0 ? msg.sender : _owner;
        timer = _timer;
        goal = _goal;
        endTimestamp = _endTimestamp;
        sum = 0;
    }

    function invest() public payable {
        require(timer.getTime() < endTimestamp, "Funding period has ended");
        require(sum < goal, "Goal has already been reached.");
        require(msg.value != 0, "Need to invest something.");
        investments[msg.sender] += msg.value;
        sum += msg.value;
    }

    function claimFunds() public {
        require(timer.getTime() >= endTimestamp, "Funding period has not ended");
        require(sum >= goal, "Goal has not been reached");
        require(msg.sender == owner, "Only owner can claim funds");
        msg.sender.transfer(sum);
    }

    function refund() public {
        require(timer.getTime() >= endTimestamp, "Funding period has not ended");
        require(sum < goal, "Goal has been reached");
        require(investments[msg.sender] != 0, "Nothing to claim");
        uint value = investments[msg.sender];
        investments[msg.sender] = 0;
        msg.sender.transfer(value);
    }
}